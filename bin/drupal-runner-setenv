#! /bin/bash
set -euf -o pipefail

echo -n "Enter local hostname: "
read HOSTNAME

echo -n "Enter environment: "
read ENVIRONMENT

source "/var/www/${HOSTNAME}/.env"

cat <<EOF > "/var/www/${HOSTNAME}/drush/sites/self.site.yml"
master:
  root: /var/www/${ENVIRONMENT}/web
  uri: ${REVIEW_PROTOCOL}://${ENVIRONMENT}.${REVIEW_DOMAIN}
  user: gitlab-runner
  host: ${REVIEW_DOMAIN}
EOF

sed -i -e "s/  SOURCE_ENVIRONMENT: SOURCE_ENVIRONMENT/  SOURCE_ENVIRONMENT: ${ENVIRONMENT}/" "/var/www/${HOSTNAME}/.gitlab-ci.yml"

drush -r "/var/www/${HOSTNAME}/web" -l "http://${HOSTNAME}" --yes --create-db sql:sync @self.master @self
drush -r "/var/www/${HOSTNAME}/web" -l "http://${HOSTNAME}" --yes cr
drush -r "/var/www/${HOSTNAME}/web" -l "http://${HOSTNAME}" --yes config-export

git -C "/var/www/${HOSTNAME}" add "./.gitlab-ci.yml"
git -C "/var/www/${HOSTNAME}" commit -m "Set drupal-runner environment."

git -C "/var/www/${HOSTNAME}" add "./drush/sites/self.site.yml"
git -C "/var/www/${HOSTNAME}" commit -m "Added Drush alias @self.master."

git -C "/var/www/${HOSTNAME}" add "./config/sync"
git -C "/var/www/${HOSTNAME}" commit -m "Initial Drupal config export."

git -C "/var/www/${HOSTNAME}" push
